﻿using UnityEngine;
using System.Collections;

public class PerlinNoise {

    private long seed;
    private int defaultChunkSize;

	public PerlinNoise(int chunkSize, long seed)
    {
        this.seed = seed;
        this.defaultChunkSize = chunkSize;
    }

    public int GetNoise(int x, int range)
    {
        int chunkSize = defaultChunkSize;
        float noise = 0;
        range /= 2;
        while (chunkSize > 0)
        {
            int chunkIndex = x / chunkSize;
            float progress = (x % chunkSize) / (chunkSize * 1f);
            float left_random = random(chunkIndex, range);
            float right_random = random(chunkIndex + 1, range);

            noise += CosineInterpolation(progress, left_random, right_random);

            chunkSize /= 2;
            range /= 2;

            range = Mathf.Max(1, range);
        }

        return (int) Mathf.Round(noise);
    }

    private float LinearInterpolation(float progress, float left, float right)
    {
        return (1 - progress) * left + progress * right;
    }

    private float CosineInterpolation(float progress, float left, float right)
    {
        float ft = progress * Mathf.PI;
        float f = (1 - Mathf.Cos(ft)) * .5f;
        return left * (1 - f) + right * f;
    }

    private int random(int x, int range)
    {
        return (int) ((x + seed) ^ 5) % range;
    }

}
