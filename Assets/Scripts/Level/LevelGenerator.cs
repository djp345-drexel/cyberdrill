﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class LevelGenerator : MonoBehaviour {

    public int seed;

    public int minY;
    public int maxY;

    public int chunkSize;

    public GameObject surface;
    public GameObject foliage;
    public List<GameObject> plants = new List<GameObject>();
    public GameObject dirt;
    public List<GameObject> ores = new List<GameObject>();

    private List<Ore> oreComponents = new List<Ore>();

    private PerlinNoise noise;

    void Start()
    {
        noise = new PerlinNoise(chunkSize, seed);
    }

    public void GenerateChunk(int chunkId)
    {
        int currentDepth = 0;
        GameObject currentTile = null;

        foreach (GameObject ore in ores)
        {
            oreComponents.Add(ore.GetComponent<Ore>());
            ore.GetComponent<Ore>().SetPrefab(ore);
        }
        for (int x = chunkId * chunkSize; x < chunkId * chunkSize + chunkSize; x++)
        {
            int columnHeight = 5 + noise.GetNoise((x + (int.MaxValue / 2)), maxY - minY - 2);
            for (int y = minY; y < columnHeight + minY; y ++)
            {
                
                if (y == columnHeight + minY - 1) //IF SURFACE
                {
                    currentTile = (GameObject) Instantiate(surface, new Vector3(x, y, 0), Quaternion.identity, transform);
                    GameObject grassTop = (GameObject) Instantiate(foliage, new Vector3(x, y + 1, 0), Quaternion.identity, transform);
                    grassTop.GetComponent<Tile>().SetChunkId(chunkId);
                    if (UnityEngine.Random.Range(0, 24) == 0)
                    {
                        GameObject flower = (GameObject) Instantiate(plants[UnityEngine.Random.Range(0, plants.Count)], new Vector3(x, y + 1, 0), Quaternion.identity, transform);
                        flower.GetComponent<Tile>().SetChunkId(chunkId);
                    }
                }
                else
                { //IF UNDERGROUND
                    currentDepth = columnHeight + minY - y - 1;
                    currentTile = (GameObject) Instantiate(dirt, new Vector3(x, y, 0), Quaternion.identity, transform);
                    foreach (Ore ore in oreComponents)
                    {
                        if (ore.InSpawnRange(currentDepth))
                        {
                            if (ore.GetFrequency() >= Random(x, y, 100) && ore.GetFrequency() != 0)
                            {
                                Destroy(currentTile);
                                currentTile = (GameObject) Instantiate(ore.GetPrefab(), new Vector3(x, y, 0), Quaternion.identity, transform);
                                break;
                            }
                        }
                    }
                }
                currentTile.GetComponent<Tile>().SetChunkId(chunkId);
            }
        }
	}

    public void UnloadChunk(int chunkId)
    {
        foreach (GameObject gameObj in FindObjectsOfType<GameObject>())
        {
            if (gameObj.GetComponent<Tile>() != null)
            {
                if (gameObj.GetComponent<Tile>().GetChunkId() == chunkId)
                {
                    Destroy(gameObj);
                }
            }
        }
    }

    private int Random(int x, int y, int range)
    {
        return (int) ((((x ^ y) * (y * 4) / Mathf.Max(x ^ 5 + 3, x) + seed) ^ 5) % range);
    }

    public int GetChunkAt(int x)
    {
        return ((int) x / chunkSize);
    }

}
