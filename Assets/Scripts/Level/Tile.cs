﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {

    private int chunkId;

    public Tile(int chunkId)
    {
        this.chunkId = chunkId;
    }

    public void SetChunkId(int chunkId)
    {
        this.chunkId = chunkId;
    }

    public int GetChunkId()
    {
        return chunkId;
    }

}
