﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChunkLoader : MonoBehaviour {

    public Camera camera;
    public LevelGenerator levelGenerator;
    public int tileSize;

    private int currentChunk;
    private int lastChunk = -(int.MaxValue) + 1;
    private int renderChunks;

    private List<int> loadedChunks = new List<int>();
    private List<int> testChunks = new List<int>();

    void Start ()
    {
        int cameraWidth = camera.pixelWidth / tileSize;
        float cameraChunks = (float) (cameraWidth * 1f / levelGenerator.chunkSize * 1f);
        renderChunks = (int)cameraChunks;
    }

	void Update ()
    {
        currentChunk = levelGenerator.GetChunkAt((int) transform.position.x);
        if (lastChunk != currentChunk)
        {
            testChunks.Clear();
            for (int x = (currentChunk - ((int) (renderChunks / 2))); x < (currentChunk + ((int) (renderChunks / 2))); x++)
            {
                if (!loadedChunks.Contains(x))
                {
                    //Debug.Log(x);
                    loadedChunks.Add(x);
                    levelGenerator.GenerateChunk(x);
                }
            }
            foreach (int chunk in loadedChunks.ToArray())
            {
                if (!(chunk >= (currentChunk - ((int)(renderChunks / 2))) && chunk <= (currentChunk + ((int)(renderChunks / 2))))) {
                    loadedChunks.Remove(chunk);
                    levelGenerator.UnloadChunk(chunk);
                }
            }
        }
        lastChunk = levelGenerator.GetChunkAt((int) transform.position.x);
	}

}
