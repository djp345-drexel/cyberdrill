﻿using UnityEngine;
using System.Collections;

public class Ore : MonoBehaviour
{
    private GameObject prefab;
    public int depth_start;
    public int depth_end;
    public int frequency;

    /*public Ore(GameObject prefab, int spawnStart, int spawnEnd, int frequency)
    {
        this.prefab = prefab;
        this.frequency = frequency;
    }*/

    public void SetPrefab(GameObject prefab)
    {
        this.prefab = prefab;
    }

    public GameObject GetPrefab()
    {
        return prefab;
    }

    public bool InSpawnRange(int depth)
    {
        return (depth >= depth_start && depth <= depth_end);
    }

    public int GetFrequency()
    {
        return frequency;
    }

}
