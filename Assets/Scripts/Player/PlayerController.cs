﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float speedx;
    public float speedy;
    public float drag;

    private GameObject drill;
    private Rigidbody2D rigidBody;
    private Animator animator;

    void Start()
    {
        drill = transform.FindChild("Drill").gameObject;
        rigidBody = this.GetComponent<Rigidbody2D>();
        animator = this.GetComponent<Animator>();
    }

	void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        if (horizontal != 0)
        {
            //if they are going to negative then horizontal must be postive
            //if they are going to positive then horizontal must be negative
            if (rigidBody.velocity.magnitude < Const.MAX_SPEED || ((rigidBody.velocity.x < 0 && horizontal > 0) || (rigidBody.velocity.x > 0 && horizontal < 0)))
            {
                rigidBody.AddForce(new Vector2(horizontal * speedx * Time.deltaTime, 0));
                animator.SetBool("walkingRight", horizontal > 0);
                drill.GetComponent<Animator>().SetBool("right", horizontal > 0);
            }
        }
        if (rigidBody.velocity.magnitude != 0 && Input.GetAxisRaw("Horizontal") == 0)
        {
            rigidBody.AddForce(new Vector2(-rigidBody.velocity.x * drag * Time.deltaTime, 0));
        }
        if (vertical != 0)
        {
            rigidBody.AddForce(new Vector2(0, vertical * speedy * Time.deltaTime));
        }
    }

}
