﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public Transform target;

    void LateUpdate ()
    {
        Vector3 newPos = target.position;
        newPos.y += 1;
        newPos.z = -10;
        transform.position = newPos;
    }

}
